self.addEventListener("install", (e) => {
  const cacheName = "tendrilicious-store";
  console.log("sw install, delete and create cache:", cacheName);
  e.waitUntil(
    caches.delete(cacheName).then(
      caches
        .open(cacheName)
        .then((cache) => cache.addAll([
        "/pwa/tendrilicious/",
        "/pwa/tendrilicious/index.html",
        "/pwa/tendrilicious/a/j/main.js",
        "/pwa/tendrilicious/a/i/icon-192x192.png",
        "/pwa/tendrilicious/a/i/icon-512x512.png",
        "/pwa/tendrilicious/a/i/flowers.webp",
      ]))
      )
  );
});
self.addEventListener("fetch", (e) => {
  console.log("sw fetch:", e.request.url);
  e.respondWith(
    caches.match(e.request).then((response) => response || fetch(e.request)),
  );
});
