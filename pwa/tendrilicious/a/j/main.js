const updateBtn = document.getElementById("updateApp");
updateBtn.addEventListener("click", e => self.location.replace("swunreg.html"));

// Register service worker to control making site work offline

if ('serviceWorker' in navigator) {
  navigator.serviceWorker
    .register('/pwa/tendrilicious/sw.js')
    .then(() => { console.log('Service Worker Registered'); });
}

// ---- mine
  const canvas = document.getElementById("canvas1");
  canvas.width = window.innerWidth;
  canvas.height= window.innerHeight;
  const ctx = canvas.getContext("2d");
  //ctx.lineWidth = 0.4;
  //ctx.globalCompositeOperation = "lighten";
  //ctx.globalCompositeOperation = "destination-over";
  class Tendril {
    constructor(x,y){
      this.x = x;
      this.y = y;
      this.speedX = Math.random() * 4 - 2;
      this.speedY = Math.random() * 4 - 2;
      this.maxSize = Math.random() * 7 + 5;
      this.size = Math.random() * 1 + 2;
      this.vs = Math.random() * 0.2 + 0.05;
      this.angleX = Math.random() * 2*Math.PI;
      this.angleY = Math.random() * 2*Math.PI;
      this.vax = Math.random() * 0.6 - 0.3;
      this.vay = Math.random() * 0.6 - 0.3;
      this.lightness = 10;
    }
    update(){
      this.x += this.speedX + Math.sin(this.angleX);
      this.y += this.speedY + Math.sin(this.angleY);
      this.size += this.vs;
      this.angleX += this.vax;
      this.angleY += this.vay;
      if(this.lightness < 70) this.lightness += 0.25;
      if(this.size < this.maxSize) {
        ctx.beginPath();
        ctx.arc(this.x, this.y, this.size, 0, 2*Math.PI);
        ctx.fillStyle = `hsl(140,100%,${this.lightness}%)`;
        ctx.fill();
        //ctx.stroke();
        requestAnimationFrame(this.update.bind(this));
      } else {
        const flower = new Flower(this.x, this.y, this.size);
        flower.grow();
      }
    }
  }
  class Flower {
    constructor(x,y,size){
      this.x = x;
      this.y = y;
      this.size = 2*size;
      this.maxFlowerSize = this.size + Math.random() * 50;
      this.vs = Math.random() * 0.3 + 0.2;
      this.image = new Image();
      this.image.src = "a/i/flowers.webp";
      this.sprite_xi = (Math.random() * 3)|0;
      this.sprite_yi = (Math.random() * 3)|0;
      this.willFlower = (this.size > 9.5);
      this.angle = Math.random() * 2*Math.PI;
      this.va = Math.random() * 0.05 - 0.025;
    }
    grow(){
      if(this.willFlower && this.size < this.maxFlowerSize){
        const sprite_size = 100;
        this.size += this.vs;
        this.angle += this.va;
        const sx = this.sprite_xi * sprite_size;
        const sy = this.sprite_yi * sprite_size;
        const size = this.size;
        ctx.save();
          ctx.translate(this.x, this.y);
          ctx.rotate(this.angle);
          ctx.drawImage(this.image, sx,sy, sprite_size, sprite_size,
            -size/2,-size/2, size,size);
        ctx.restore();
        requestAnimationFrame(this.grow.bind(this));
      }
    }
  }
  function mousemove(e){
    for(let i=0; i<3; ++i){
      const tendril = new Tendril(e.offsetX, e.offsetY);
      tendril.update();
    }
  }
  function mousedown(e){
    canvas.addEventListener("pointermove", mousemove);
  }
  function mouseup(e){
    canvas.removeEventListener("pointermove", mousemove);
  }
  function init(e){
    canvas.addEventListener("pointerdown", mousedown);
    canvas.addEventListener("pointerup", mouseup);
  }
  init();
